package Controlador;

import Excepciones.*;
import Modelo.*;
import Persistencia.IOPedidosRestoran;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;


/*Aportaron: Sebastian Fierro S.
             Bruno Caro V.
             Miguel Raibel B.
             Natalia Carrasco C.
*/

public class ControladorPedidosRestoran {
    private static ControladorPedidosRestoran instance = null;

    private ControladorPedidosRestoran(){
        clientes = new ArrayList<>();
        repartidores = new ArrayList<>();
        productos = new ArrayList<>();
        pedidos = new ArrayList<>();
    }
    public static ControladorPedidosRestoran getInstance(){
        if (instance == null){
            instance = new ControladorPedidosRestoran();
        }
        return instance;
    }
    private ArrayList<Cliente> clientes;
    private ArrayList<Repartidor> repartidores;
    private ArrayList<Producto>  productos;
    private ArrayList<Pedido> pedidos;

    public int addPedido(String rutCliente, String[][] datosProductos,boolean conComprobante) throws ClienteNoEncontradoException, ProductoNoEncontradoException, ComprobantePedidoNoGenerado, PedidoSinStockSuficiente, ValorInvalidoException, OperacionInvalidaException, ObjetoRepetidoException {
        int numeroPedido=0;
        if(buscarCliente(rutCliente)!=null){
            LocalDate fecha=LocalDate.now();
            Pedido pedido = new Pedido(pedidos.size()+1,fecha,buscarCliente(rutCliente));
            for (int i = 0; i < datosProductos.length; i++) {
                if(buscarProducto(Integer.parseInt(datosProductos[i][0]))!=null) {
                    buscarProducto(Integer.parseInt(datosProductos[i][0])).addLineaPedido(new LineaPedido(Integer.parseInt(datosProductos[i][1]), buscarProducto(Integer.parseInt(datosProductos[i][0])), pedido));
                    pedido.addProducto(buscarProducto(Integer.parseInt(datosProductos[i][0])), Integer.parseInt(datosProductos[i][1]));
                }else{
                    throw new ProductoNoEncontradoException("No existe el o los productos indicados");
                }
                numeroPedido=pedido.getNumero();
            }
            buscarCliente(rutCliente).addPedido(pedido);

            if(conComprobante) {
                try {
                    IOPedidosRestoran.getInstance().saveComprobantePedido(pedido);
                }catch (FileNotFoundException e){
                    throw new ComprobantePedidoNoGenerado("No se pudo generar el comprobante");
                }
            }
            pedidos.add(pedido);
            return numeroPedido;
        }else{
            throw new ClienteNoEncontradoException("No existe cliente con el rut ingresado");
        }

    }
    public void addCliente(String rut, String nombre, String domicilio, String telefono) throws ObjetoRepetidoException, OperacionInvalidaException {
        if(!rut.isBlank() & !rut.isEmpty()){
            if(!nombre.isBlank() & !nombre.isEmpty()){
                if(!domicilio.isBlank() & !domicilio.isEmpty()){
                    if(!telefono.isBlank() & !telefono.isEmpty()){
                        if(buscarCliente(rut) == null){
                            clientes.add(new Cliente(rut, nombre, domicilio, telefono));
                        }else{
                            throw new ObjetoRepetidoException("Cliente ya existe");
                        }
                    }else{
                        throw new OperacionInvalidaException("Debe ingresar un telefono!");
                    }
                }else{
                    throw new OperacionInvalidaException("Debe ingresar un domicilio!");
                }
            }else{
                throw new OperacionInvalidaException("Debe ingresar un nombre!");
            }
        }else{
            throw new OperacionInvalidaException("Debe ingresar un rut!");
        }
    }
    public void addRepartidor(String rut, String nombre, String domicilio, String telefono) throws ObjetoRepetidoException, OperacionInvalidaException {
        if(!rut.isBlank() & !rut.isEmpty()){
            if(!nombre.isBlank() & !nombre.isEmpty()){
                if(!domicilio.isBlank() & !domicilio.isEmpty()){
                    if(!telefono.isBlank() & !telefono.isEmpty()){
                        if(buscarRepartidor(rut)==null){
                            repartidores.add(new Repartidor(rut, nombre, domicilio, telefono));
                        }else{
                            throw new ObjetoRepetidoException("Repartidor ya existe");
                        }
                    }else{
                        throw new OperacionInvalidaException("Debe ingresar un telefono!");
                    }
                }else{
                    throw new OperacionInvalidaException("Debe ingresar un domicilio!");
                }
            }else{
                throw new OperacionInvalidaException("Debe ingresar un nombre!");
            }
        }else{
            throw new OperacionInvalidaException("Debe ingresar un rut!");
        }
    }
    public void addProductoBasico(int codigo, String nombre, int precioCosto, int margenVenta) throws ValorInvalidoException, ObjetoRepetidoException {//AGREGAR EXCEPCION
        if(buscarProducto(codigo)==null){
            if((int)(precioCosto/(1-(margenVenta/100))) > 0 ){
                if(margenVenta<100){
                    if(margenVenta>0){
                        productos.add(new ProductoBasico(codigo, nombre, precioCosto, margenVenta));
                    }else{
                        throw new ValorInvalidoException("Margen de venta menor o igual a 0");
                    }
                }else{
                    throw new ValorInvalidoException("Valor margen de ventas superior al 100%");
                }
            }else{
                throw new ValorInvalidoException("Precio venta menor 0");
            }
        }else{
            throw new ObjetoRepetidoException("Producto ya existe");
        }
    }
    public void addCombo(int codigo, String nombre, int margenVenta, int[] codigosProductos) throws ProductoNoEncontradoException, ValorInvalidoException, ObjetoRepetidoException {
        if(buscarProducto(codigo)==null){
            if(margenVenta<100){
                if(margenVenta>0){
                    Combo combo = new Combo(codigo,nombre,margenVenta);
                    for(int i=0; i<codigosProductos.length; i++){
                        if(buscarProducto(codigosProductos[i])!=null) {
                            combo.addProducto(buscarProducto(codigosProductos[i]));
                        }else{
                            throw new ProductoNoEncontradoException("El codigo del producto elegido no existe");
                        }
                    }
                    productos.add(combo);
                }else{
                    throw new ValorInvalidoException("Margen de venta menor o igual a 0");
                }
            }else{
                throw new ValorInvalidoException("Valor margen de ventas superior al 100%");
            }
        }else{
            throw new ObjetoRepetidoException("Producto ya existe");
        }
    }
    public void addStockAProducto(int codigo, int nroUnidades) throws ValorInvalidoException, ProductoNoEncontradoException {//AGREGAR EXCEPCION
        if(buscarProducto(codigo) !=null){
            if(nroUnidades>0){
                buscarProducto(codigo).addStock(nroUnidades);
            }else{
                throw new ValorInvalidoException("Numero de unidades menor o igual a 0");
            }
        }else{
            throw new ProductoNoEncontradoException("No existe el producto con el codigo ingresado");
        }
    }
    public void setRepartidorAPedido(String rut, int codigo) throws PedidoNoEncontradoException, RepartidorNoEncontradoException {//AGREGAR EXCEPCION
        if(buscarRepartidor(rut) != null){
            if(buscarPedido(codigo)!= null){
                buscarPedido(codigo).setRepartidor(buscarRepartidor(rut));
                try {
                    buscarRepartidor(rut).addPedido(buscarPedido(codigo));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }else{
                throw new PedidoNoEncontradoException("No existe pedido con el codigo ingresado");
            }
        }else{
            throw new RepartidorNoEncontradoException("No existe repartidor con el rut ingresado");
        }
    }
    public void setEntregaPedido(int numero) throws OperacionInvalidaException, PedidoNoEncontradoException {
        if(buscarPedido(numero)!=null){
            if(buscarPedido(numero).getEstado() == Pedido.Estado.PENDIENTE){
                buscarPedido(numero).setEntregado();
                System.out.println("El pedido " + numero + " se ha entregado con exito");
            }else{
                throw new OperacionInvalidaException("El pedido ya fue entregado");
            }
        }else{
            throw new PedidoNoEncontradoException("No existe pedido con el codigo ingresado");
        }
    }
    public String[][] listProductos(){
        if(productos.size()>0){
            String [][] lista = new String [productos.size()][6];
            int i=0;
            for (Producto productos : productos){
                String[] productoStrng;
                try {
                    productoStrng = getDatosProducto(productos.getCodigo());
                    lista[i][0] = productoStrng[0];
                    lista[i][1] = productoStrng[1];
                    lista[i][2] = productoStrng[2];
                    lista[i][3] = productoStrng[3];
                    lista[i][4] = productoStrng[4];
                    lista[i][5] = String.valueOf(productos.getNroProductos());
                    i++;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return lista;
        }else{
            return new String [0][0];
        }
    }
    public String [][] listRepartidores(){
        if(repartidores.size()>0){
            String [][] listaRepartidor = new String[repartidores.size()][4];
            int i=0;
            for (Repartidor repartidor : repartidores) {
                String[] repartidorStrng;
                try {
                    repartidorStrng = getDatosRepartidor(repartidor.getRut());
                    listaRepartidor[i][0] = repartidorStrng[0];
                    listaRepartidor[i][1] = repartidorStrng[1];
                    listaRepartidor[i][2] = repartidorStrng[2];
                    listaRepartidor[i][3] = repartidorStrng[3];
                    i++;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            return listaRepartidor;
        }else{
            return new String[0][0];
        }
    }
    public String[][] listPedidosPendientes(String rutCliente) throws ClienteNoEncontradoException { //Pedidos sin entregar del cliente  AGREGAR EXCEPCION
        if(buscarCliente(rutCliente)!=null){
            if(buscarCliente(rutCliente).getPedidosPendientes().length>0){
                String [][] pedidosPorRecibir = new String[buscarCliente(rutCliente).getPedidosPendientes().length][4];
                int i = 0;
                for(Pedido pedido : buscarCliente(rutCliente).getPedidosPendientes()){
                    pedidosPorRecibir [i][0] = String.valueOf(pedido.getNumero());
                    pedidosPorRecibir [i][1] = String.valueOf(pedido.getFecha());
                    pedidosPorRecibir [i][2] = pedido.getRepartidor().getRut();
                    pedidosPorRecibir [i][3] = pedido.getRepartidor().getNombre();
                    i++;
                }
                return pedidosPorRecibir;
            }else {
                return new String[0][0];
            }
        }else{
            throw new ClienteNoEncontradoException("No existe cliente con el rut ingresado");
        }
    }
    public String[][] listPedidosPorEntregar(String rutRepartidor) throws RepartidorNoEncontradoException {
        if(buscarRepartidor(rutRepartidor)!=null){
            if (buscarRepartidor(rutRepartidor).getPedidosPendientes().length>0){
                String [][] pedidosRepartir = new String[buscarRepartidor(rutRepartidor).getPedidosPendientes().length][4];
                int i = 0;
                for(Pedido pedido : buscarRepartidor(rutRepartidor).getPedidosPendientes()){
                    pedidosRepartir [i][0] = String.valueOf(pedido.getNumero());//Numeropedido
                    pedidosRepartir [i][1] = String.valueOf(pedido.getFecha());//fechaPedido
                    pedidosRepartir [i][2] = pedido.getCliente().getRut();//RutCliente
                    pedidosRepartir [i][3] = pedido.getCliente().getNombre();//Nombre cliente
                    i++;
                }
                return pedidosRepartir;
            }else{
                return new String[0][0];
            }
        }else{
            throw new RepartidorNoEncontradoException("No existe repartidor con el rut ingresado");
        }
    }
    public String[][] ProductosMasSolicitados(int nroPedidosMin) throws ValorInvalidoException {
        if(nroPedidosMin>=0){
            int contador = 0;
            if (productos.size() > 0) {
                for (Producto producto : productos){
                    if(producto.getNroLineasPedidos() >= nroPedidosMin){
                        contador++;
                    }
                }
                int i=0;
                String[][] productoSolicitado = new String[contador][4];
                for (Producto producto: productos){
                    if(producto.getNroLineasPedidos() >= nroPedidosMin){
                        productoSolicitado[i][0]=String.valueOf(producto.getCodigo());
                        productoSolicitado[i][1]=producto.getNombre();
                        productoSolicitado[i][2]=String.valueOf(producto.getPrecioVenta());
                        productoSolicitado[i][3]=String.valueOf(producto.getNroLineasPedidos());
                        i++;
                    }
                }
                return productoSolicitado;
            }
            return new String[0][0];
        }else{
            throw new ValorInvalidoException("Numero menor que 0");
        }
    }
    //Obtener datos especificos
    public String[] getDatosCliente(String rut) throws ClienteNoEncontradoException {
        if(buscarCliente(rut)!=null){
            String[] datosClientes = new String[4];
            if (buscarCliente(rut) != null) {
                datosClientes = buscarCliente(rut).toString().split(",");
            }
            return datosClientes;
        }else{
            throw new ClienteNoEncontradoException("No existe cliente con el rut ingresado");
        }
    }
    public String[] getDatosProducto(int codigo) throws ProductoNoEncontradoException {//AGREGAR EXCEPCION
        if(buscarProducto(codigo)!=null){
            String[] datosProducto = new String[5];
            if(buscarProducto(codigo)!=null){
                datosProducto = buscarProducto(codigo).toString().split(",");
            }
            return datosProducto;
        }else{
            throw new ProductoNoEncontradoException("No existe producto con el codigo ingresado");
        }
    }
    public String[] getDatosRepartidor(String rut) throws RepartidorNoEncontradoException {//AGREGAR EXCEPCION
        if(buscarRepartidor(rut)!=null){
            String[] datosRepartidor = new String[4];
            if(buscarRepartidor(rut)!=null){
                datosRepartidor = buscarRepartidor(rut).toString().split(",");
            }
            return datosRepartidor;
        }else{
            throw new RepartidorNoEncontradoException("No existe repartidor con el rut ingresado");
        }
    }

    public void readDatosSistema()throws OperacionInvalidaException{
        Producto[] productArr;
        Cliente[] clientArr;
        Repartidor[] reparArr;
        Pedido[] pediArr;
        try{
            productArr= IOPedidosRestoran.getInstance().readProductosSinPedidos();
            clientArr = IOPedidosRestoran.getInstance().readClientesSinPedidos();
            reparArr = IOPedidosRestoran.getInstance().readRepartidoresSinPedidos();
            pediArr = IOPedidosRestoran.getInstance().readPedidos();
            productos.clear();
            clientes.clear();
            repartidores.clear();
            pedidos.clear();
            for(Producto producto: productArr){
                productos.add(producto);
            }
            for(Cliente cliente: clientArr){
                clientes.add(cliente);
            }
            for(Repartidor repartidor: reparArr){
                repartidores.add(repartidor);
            }
            for(Pedido pedido: pediArr){
                //añadir los clientes, productos y repartidores de los pedidos.
                pedidos.add(pedido);
                if(!clientes.contains(pedido.getCliente())){
                    clientes.add(pedido.getCliente());
                }
                if(!pedido.getRepartidor().getNombre().equals("-")){
                    if(!repartidores.contains(pedido.getRepartidor())){
                        repartidores.add(pedido.getRepartidor());
                    }
                }
                LineaPedido[] producto = pedido.getLineasPedido();
                for(LineaPedido linea: producto){
                    if(!productos.contains(linea.getProducto())){
                        productos.add(linea.getProducto());
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new OperacionInvalidaException("No es posible leer los datos");
        }
    }
    public void saveDatosSistema()throws OperacionInvalidaException{
        try{
            IOPedidosRestoran.getInstance().savePedidos(pedidos.toArray(new Pedido[0]));
            /*ArrayList<Producto> prodSinPed = new ArrayList<>();
            for(Producto prod : productos){
                if(prod.getNroLineasPedidos()==0){
                    prodSinPed.add(prod);
                }
            }
            ArrayList<Cliente> cliSinPed = new ArrayList<>();
            for (Cliente cli : clientes){
                if(cli.getNroPedidos()==0){
                    cliSinPed.add(cli);
                }
            }
            ArrayList<Repartidor> repSinPed = new ArrayList<>();
            for (Repartidor rep : repartidores){
                if(rep.getNroPedidos()==0){
                    repSinPed.add(rep);
                }
            }*/
            IOPedidosRestoran.getInstance().saveProductosYPersonasSinPedidos(productos.toArray(new Producto[0]),
                    clientes.toArray(new Cliente[0]),repartidores.toArray(new Repartidor[0]));
        }catch(IOException e){
            throw new OperacionInvalidaException("No es posible guardar los datos");
        }
    }
    private Cliente buscarCliente (String rut){
        for (Cliente cliente : clientes){
            if(cliente.getRut().equals(rut)){
                return cliente;
            }
        }
        return null;
    }
    private Producto buscarProducto(int codigo){
        //precondicion: codigo>0
        for (Producto producto : productos){
            if(producto.getCodigo() == codigo){
                return producto;
            }
        }
        return null;
    }

    private Repartidor buscarRepartidor(String rut){
        for (Repartidor repartidor : repartidores){
            if(repartidor.getRut().equals(rut)){
                return repartidor;
            }
        }
        return null;
    }
    private Pedido buscarPedido(int numero){
        //precondicion: numero>0
        for(Pedido pedido : pedidos){
            if(pedido.getNumero()==numero){
                return pedido;
            }
        }
        return null;
    }
}
