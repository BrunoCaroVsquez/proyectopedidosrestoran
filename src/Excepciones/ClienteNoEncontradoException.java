package Excepciones;

public class ClienteNoEncontradoException extends Exception{
    public ClienteNoEncontradoException(String msg){
        super(msg);
    }
}
