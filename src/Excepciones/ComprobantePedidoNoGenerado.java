package Excepciones;

public class ComprobantePedidoNoGenerado extends Exception{
    public ComprobantePedidoNoGenerado(String msg){
        super(msg);
    }
}
