package Excepciones;

public class ObjetoRepetidoException extends Exception{
    public ObjetoRepetidoException(String msg){
        super(msg);
    }

}
