package Excepciones;

public class PedidoNoEncontradoException extends Exception{
    public PedidoNoEncontradoException(String msg){
        super(msg);
    }
}
