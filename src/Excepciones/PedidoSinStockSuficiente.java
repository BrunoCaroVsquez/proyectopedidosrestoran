package Excepciones;

public class PedidoSinStockSuficiente extends Exception{
    public PedidoSinStockSuficiente(String msg){
        super(msg);
    }
}
