package Excepciones;

public class ProductoNoEncontradoException extends Exception{
    public ProductoNoEncontradoException(String msg){
        super(msg);
    }
}

