package Excepciones;

public class RepartidorNoEncontradoException extends Exception{
    public RepartidorNoEncontradoException(String msg){
        super(msg);
    }
}
