package Modelo;

import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/* Aportaron: Miguel Raibel B.
              Bruno Caro V.
              Sebastian Fierro S.

 */

public class Cliente extends Persona {

    private ArrayList<Pedido> pedidos;

    public Cliente(String rut, String nombre, String domicilio, String telefono){
        super(rut,nombre,domicilio,telefono);
        pedidos=new ArrayList<>();
    }

    @Override
    public void addPedido(Pedido pedido) throws ValorInvalidoException, ObjetoRepetidoException {
        int rep = 0;
        for (Pedido pedido1 : pedidos){
            if(pedido == pedido1){
                rep = 1;
            }
        }
        if(rep != 1){
            if(pedido.getCliente()!= null){
                pedidos.add(pedido);
            }else{
                throw new ValorInvalidoException("Pedido asociado a otro cliente");
            }
        }else{
            throw new ObjetoRepetidoException("Pedido esta repetido");
        }
    }

    @Override
    public Pedido[] getPedidosPendientes() {
        if (pedidos.size()>0){
            int i = 0;
            int j = 0;
            for (Pedido pedido: pedidos){
                if(pedido.getEstado() == Pedido.Estado.PENDIENTE ){
                    i++;
                }
            }
            Pedido [] pedidosPendientes = new Pedido[i];
            for (Pedido pedido: pedidos){
                if(pedido.getEstado() == Pedido.Estado.PENDIENTE ){
                    pedidosPendientes[j] = pedido;
                    j++;
                }
            }
            return pedidosPendientes;
        }else{
            return new Pedido[0];
        }
    }

    @Override
    public int getNroPedidos() {
        return pedidos.size();
    }
}


