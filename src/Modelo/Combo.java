package Modelo;

import Excepciones.ProductoNoEncontradoException;

import java.util.ArrayList;

public class Combo extends Producto{

    private ArrayList<Producto> productos;

    public Combo(int codigo, String nombre, int margenVenta){
        super(codigo,nombre,margenVenta);
        productos = new ArrayList<>();

    }

    @Override
    public int getPrecioCosto() {
        int precioCosto = 0;
        for(Producto producto: productos){
            precioCosto += producto.getPrecioCosto();
        }
        return precioCosto;
    }

    @Override
    public void addProducto(Producto producto){
        productos.add(producto);
    }

    @Override
    public Producto getProducto(int posicion){
        return productos.get(posicion);
    }

    public int getNroProductos(){
        return productos.size();
    }
}
