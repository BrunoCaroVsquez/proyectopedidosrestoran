package Modelo;

import Excepciones.ValorInvalidoException;

import java.io.Serializable;

/*Aportaron: Bruno Caro V.
             Natalia Carrasco C.

*/
public class LineaPedido implements Serializable {
    private int cantidad;
    private Pedido pedido;
    private Producto producto;

    public LineaPedido(int cantidad, Producto producto, Pedido pedido) throws ValorInvalidoException { //AGREGAR EXCEPCION
        if(cantidad>=0){
            this.cantidad=cantidad;
        }else{
            throw new ValorInvalidoException("Cantidad menor que 0");
        }
        this.producto=producto;
        this.pedido=pedido;
    }

    public int getSubtotal(){
        return cantidad*(producto.getPrecioVenta());
    }

    @Override
    public String toString() {
        return cantidad + "," + pedido + "," + producto;
    }

    public Producto getProducto(){
        return producto;
    }
}
