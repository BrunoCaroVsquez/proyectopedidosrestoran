package Modelo;

import Excepciones.OperacionInvalidaException;
import Excepciones.PedidoSinStockSuficiente;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/*Aportaron: Bruno Caro V.
           Natalia Carrasco C.
           Miguel Raibel B.
           Sebastian Fierro S.
*/

public class Pedido implements Serializable {
    public static enum Estado{
        ENTREGADO,
        PENDIENTE
    }
    private int numero;
    private LocalDate fecha;
    private Estado estado;

    private Cliente cliente;
    private Repartidor repartidor;
    private ArrayList <LineaPedido> lineaPedido;

    public Pedido(int numero, LocalDate fecha, Cliente cliente){
        this.numero=numero;
        this.fecha=fecha;
        this.cliente=cliente;
        this.estado= Estado.PENDIENTE;
        lineaPedido=new ArrayList<>();
        repartidor = new Repartidor("-","-","-","-");
    }

    public int getNumero() {

        return numero;
    }

    public String getFecha() {

        return fecha.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public Estado getEstado(){

        return estado;
    }

    public Cliente getCliente() {

        return cliente;
    }

    public Repartidor getRepartidor() {

        return repartidor;
    }

    public void addProducto(Producto producto, int cantidad) throws ValorInvalidoException, OperacionInvalidaException, PedidoSinStockSuficiente {
        if(estado == Estado.PENDIENTE){
            if(cantidad>0){
                try{
                    lineaPedido.add(new LineaPedido(cantidad, producto, this));
                    producto.removeStock(cantidad);
                }catch (ValorInvalidoException e){
                    throw new ValorInvalidoException(e.getMessage());
                }catch (PedidoSinStockSuficiente e){
                    throw new PedidoSinStockSuficiente(e.getMessage());
                }
            }else{
                throw new ValorInvalidoException("Cantidad menor o igual que 0");
            }
        }else{
            throw new OperacionInvalidaException("El pedido ya ha sido entregado");
        }
    }

    public int getMontoTotal(){
        int montoTotal=0;
        for(LineaPedido lineas: lineaPedido){
            montoTotal+=lineas.getSubtotal();
        }
        return montoTotal;
    }

    public void setRepartidor(Repartidor repartidor){

        this.repartidor=repartidor;
    }

    public void setEntregado(){

        this.estado = Estado.ENTREGADO;
    }

    public LineaPedido[] getLineasPedido(){
        return lineaPedido.toArray(new LineaPedido[0]);
    }



}
