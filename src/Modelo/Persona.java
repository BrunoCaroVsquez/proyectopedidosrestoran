package Modelo;

import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.Objects;

public abstract class Persona implements Serializable {

    private String rut;
    private String nombre;
    private String direccion;
    private String telefono;

    public Persona(String rut, String nombre, String direccion, String telefono){
        this.nombre=nombre;
        this.direccion=direccion;
        this.telefono=telefono;
        this.rut=rut;
    }
    public String getRut(){
        return rut;
    }

    public String getNombre() {
        return nombre;
    }

    public static boolean validaRut(String rut){
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
        char dv = rut.charAt(rut.length() - 1);
        int suma = 0;
        for(int mult=2;rutAux>0;mult++){
            int digito = rutAux%10;
            rutAux/=10;
            suma += digito*mult;
            if(mult>=7){
                mult = 1;
            }
        }
        int resto = suma%11;
        int resultado = 11-resto;
        String comparador;
        if(resultado<10){
            comparador = String.valueOf(resultado);
        }else{
            if(resultado == 10){
                comparador = "k";
            }else{
                comparador = "0";
            }
        }
        if(comparador.equalsIgnoreCase(String.valueOf(dv))){
            return true;
        }else{
            return false;
        }
    }

    public abstract void addPedido(Pedido pedido)throws ValorInvalidoException, ObjetoRepetidoException;

    public abstract Pedido[] getPedidosPendientes();

    public abstract int getNroPedidos();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Objects.equals(rut, persona.rut);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rut);
    }

    @Override
    public String toString() {
        return rut + "," + nombre + "," + direccion + "," + telefono;
    }
}
