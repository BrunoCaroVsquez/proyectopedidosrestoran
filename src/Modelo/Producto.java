package Modelo;

import Excepciones.PedidoSinStockSuficiente;
import Excepciones.ProductoNoEncontradoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/*Aportaron: Bruno Caro V.
             Miguel Raibel B.
 */

public abstract class Producto implements Serializable {
    private int codigo;
    private String nombre;
    private int margenVenta;
    private int stock;

    private ArrayList<LineaPedido> lineaPedido;

    public Producto(int codigo, String nombre, int margenVenta){
        this.codigo=codigo;
        this.nombre=nombre;
        this.margenVenta=margenVenta;
        this.stock=0;
        lineaPedido=new ArrayList<>();
    }

    public int getCodigo() {

        return codigo;
    }

    public String getNombre() {

        return nombre;
    }

    public abstract int getPrecioCosto();

    public int getPrecioVenta(){

        return (int) (getPrecioCosto()/(1-(margenVenta/100)));
    }

    public void addLineaPedido(LineaPedido lineaPedido){

        this.lineaPedido.add(lineaPedido);
    }

    public int getNroLineasPedidos(){
        int contadorLineas=0;
        for(LineaPedido lineas: lineaPedido){
            contadorLineas++;
        }
        return contadorLineas;
    }

    public void addStock(int nroUnidades) throws ValorInvalidoException {
        if(nroUnidades>0){
            this.stock+=nroUnidades;
        }else{
            throw new ValorInvalidoException("Valor menor que 0");
        }
    }
    public void removeStock(int nroUnidades) throws ValorInvalidoException, PedidoSinStockSuficiente {//AGREGAR EXCEPCION
        if(nroUnidades>0 && nroUnidades<stock){
            stock-=nroUnidades;
        }else{
            if(nroUnidades<=0){
                throw new ValorInvalidoException("Valor fuera de rango");
            }else{
                if(nroUnidades>stock){
                    throw new PedidoSinStockSuficiente("Valor fuera de rango");
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return codigo == producto.codigo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public String toString() {

        return codigo + "," + nombre + "," + getPrecioCosto() + "," + margenVenta +  "," + stock;
    }

    public void addProducto(Producto producto) throws ProductoNoEncontradoException {
        throw new ProductoNoEncontradoException("No se puede realizar la operacion");
    }
    public Producto getProducto(int posicion) throws ProductoNoEncontradoException{
        throw new ProductoNoEncontradoException("No es posible continuar");
    }
    public int getNroProductos(){
        return 0;
    }
}
