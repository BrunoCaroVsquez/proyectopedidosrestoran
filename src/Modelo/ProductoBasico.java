package Modelo;

public class ProductoBasico extends Producto{
    private int precioCosto;

    public ProductoBasico(int codigo, String nombre, int precioCosto, int margenVenta){
        super(codigo,nombre,margenVenta);
        this.precioCosto=precioCosto;
    }

    @Override
    public int getPrecioCosto() {
        return precioCosto;
    }


}
