package Modelo;

import Excepciones.ObjetoRepetidoException;
import Excepciones.ValorInvalidoException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/*Aportaron: Bruno Caro V.
             Miguel Raibel B.
*/
public class Repartidor extends Persona {

    private ArrayList<Pedido> pedidos;

    public Repartidor(String rut, String nombre, String domicilio, String telefono){
        super(rut,nombre,domicilio,telefono);
        pedidos=new ArrayList<>();
    }

    @Override
    public void addPedido(Pedido pedido) throws ObjetoRepetidoException, ValorInvalidoException {
        int rep = 0;
        for (Pedido pedido1 : pedidos){
            if(pedido == pedido1){
                rep = 1;
            }
        }
        if(rep != 1){
            if(pedido.getRepartidor()!= null){
                pedidos.add(pedido);
            }else{
                throw new ValorInvalidoException("Pedido asociado a otro repartidor");
            }
        }else{
            throw new ObjetoRepetidoException("Pedido esta repetido");
        }
    }

    @Override
    public Pedido[] getPedidosPendientes() {
        if (pedidos.size()>0){
            int i = 0;
            int j = 0;
            for (Pedido pedido: pedidos){
                if(pedido.getEstado() == Pedido.Estado.PENDIENTE ){
                    i++;
                }
            }
            Pedido [] pedidosARepartir = new Pedido[i];
            for (Pedido pedido: pedidos){
                if(pedido.getEstado() == Pedido.Estado.PENDIENTE ){
                    pedidosARepartir[j] = pedido;
                    j++;
                }
            }
            return pedidosARepartir;
        }else{
            return new Pedido[0];
        }
    }

    @Override
    public int getNroPedidos(){
        return pedidos.size();
    }


}
