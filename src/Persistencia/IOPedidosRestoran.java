package Persistencia;

import Modelo.*;

import java.io.*;
import java.util.ArrayList;

public class IOPedidosRestoran {
    private static IOPedidosRestoran instance = null;

    private IOPedidosRestoran(){}

    public static IOPedidosRestoran getInstance(){
        if(instance==null){
            instance = new IOPedidosRestoran();
        }
        return instance;
    }

    public Producto[] readProductosSinPedidos() throws IOException, ClassNotFoundException{
        ArrayList<Producto> productos = new ArrayList<>();
        ObjectInputStream prodArch = new ObjectInputStream( new FileInputStream("productos.obj"));
        try{
            do{
                productos.add((Producto) prodArch.readObject());
            }while(true);
        }catch (EOFException e){
            prodArch.close();
            return productos.toArray(new Producto[0]);
        }
    }
    public Cliente[] readClientesSinPedidos() throws IOException, ClassNotFoundException{
        ArrayList<Cliente> clientes = new ArrayList<>();
        ObjectInputStream cliArch = new ObjectInputStream( new FileInputStream("clientes.obj"));
        try{
            do{
                clientes.add((Cliente) cliArch.readObject());
            }while(true);
        }catch (EOFException e){
            cliArch.close();
            return clientes.toArray(new Cliente[0]);
        }
    }
    public Repartidor[] readRepartidoresSinPedidos() throws IOException, ClassNotFoundException{
        ArrayList<Repartidor> repartidores = new ArrayList<>();
        ObjectInputStream repArch = new ObjectInputStream( new FileInputStream("repartidores.obj"));
        try{
            do{
                repartidores.add((Repartidor) repArch.readObject());
            }while(true);
        }catch (EOFException e){
            repArch.close();
            return repartidores.toArray(new Repartidor[0]);
        }
    }
    public Pedido[] readPedidos() throws IOException, ClassNotFoundException {
        ArrayList<Pedido> pedidos = new ArrayList<>();
        ObjectInputStream pedArch = new ObjectInputStream( new FileInputStream("pedidos.obj"));
        try{
            do{
                pedidos.add((Pedido) pedArch.readObject());
            }while(true);
        }catch (EOFException e){
            pedArch.close();
            return pedidos.toArray(new Pedido[0]);
        }
    }
    public void saveProductosYPersonasSinPedidos(Producto[] producto, Cliente[] cliente, Repartidor[] repartidor) throws IOException{
        ObjectOutputStream prodArch = new ObjectOutputStream( new FileOutputStream("productos.obj"));
        for (Producto producto1 : producto){
            if(producto1.getNroLineasPedidos()==0){
                prodArch.writeObject(producto1);
            }

        }
        prodArch.close();
        ObjectOutputStream cliArch = new ObjectOutputStream( new FileOutputStream("clientes.obj"));
        for (Cliente cliente1 : cliente){
            if(cliente1.getNroPedidos()==0){
                cliArch.writeObject(cliente1);
            }

        }
        cliArch.close();
        ObjectOutputStream repArch = new ObjectOutputStream( new FileOutputStream("repartidores.obj"));
        for (Repartidor repartidor1 : repartidor){
            if(repartidor1.getNroPedidos()==0){
                repArch.writeObject(repartidor1);
            }
        }
        repArch.close();
    }
    public void savePedidos(Pedido[] pedido) throws IOException {
        ObjectOutputStream pedArch = new ObjectOutputStream( new FileOutputStream("pedidos.obj"));
        for(Pedido pedido1 : pedido){
            pedArch.writeObject(pedido1);
        }
        pedArch.close();
    }
    public void saveComprobantePedido(Pedido pedido) throws FileNotFoundException {
        LineaPedido[] datosPedido = pedido.getLineasPedido();
        String[][] DatosPedido = new String[datosPedido.length][4];
        for (int i = 0; i < datosPedido.length; i++) {
            DatosPedido[i][0] = datosPedido[i].getProducto().getNombre();
            DatosPedido[i][1] = String.valueOf(datosPedido[i].getSubtotal() / datosPedido[i].getProducto().getPrecioVenta());
            DatosPedido[i][2] = String.valueOf(datosPedido[i].getProducto().getPrecioVenta());
            DatosPedido[i][3] = String.valueOf(datosPedido[i].getSubtotal());
        }
        File file = new File("Pedido "+pedido.getNumero()+".txt");
        PrintStream pw = new PrintStream(new FileOutputStream(file));
        pw.println("\t\tPedido Nro. "+ pedido.getNumero());
        pw.println("Fecha: " + pedido.getFecha());
        pw.println("Cliente: " + pedido.getCliente().getNombre());
        pw.println("\n-----------------------------  DETALLE PRODUCTOS  ----------------------------------\n");
        pw.printf("%-10s %-30s %10s %15s %15s%n","Numero","Nombre Producto","Cantidad","Precio","Subtotal");
        int j = 1;
        for(String[] datos: DatosPedido){
            pw.printf("%-10d %-30s %10s %15s %15s%n", j ,datos[0],datos[1],datos[2],datos[3]);
            j++;
        }
        pw.println("------------------------------------------------------------------------------------");
        pw.printf("%75s %8d%n","Total $",pedido.getMontoTotal());
        pw.printf("%84s","====================");
        pw.flush();
        pw.close();
    }
}
