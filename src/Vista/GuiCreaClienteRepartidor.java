package Vista;

import Controlador.ControladorPedidosRestoran;
import Excepciones.ObjetoRepetidoException;
import Excepciones.OperacionInvalidaException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import static Modelo.Persona.validaRut;

public class GuiCreaClienteRepartidor extends JFrame implements ActionListener {
    private JLabel rutLabel, nombreLabel, domicilioLabel, telefonoLabel;
    private JTextField rutField, nombreField, domicilioField, telefonoField;
    private JPanel datosPanel, botonesPanel, opcionesPanel;
    private JButton aceptar, cancelar;
    private JRadioButton clienteButton, repartidorButton;
    private ButtonGroup opciones;

    public GuiCreaClienteRepartidor(String title){
        super(title);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(500,250);

    }


    public void inicia(){
        nombreLabel = new JLabel("Nombre", SwingConstants.RIGHT);
        rutLabel = new JLabel("Rut", SwingConstants.RIGHT);
        domicilioLabel = new JLabel("Domicilio", SwingConstants.RIGHT);
        telefonoLabel = new JLabel("Teléfono", SwingConstants.RIGHT);
        nombreField = new JTextField(15);
        rutField = new JTextField("P.e 11.111.111-1",15);
        domicilioField = new JTextField(15);
        telefonoField = new JTextField(15);

        datosPanel = new JPanel();
        datosPanel.setLayout(new GridLayout(4,2,5,5));
        datosPanel.add(nombreLabel);
        datosPanel.add(nombreField);
        datosPanel.add(rutLabel);
        datosPanel.add(rutField);
        datosPanel.add(domicilioLabel);
        datosPanel.add(domicilioField);
        datosPanel.add(telefonoLabel);
        datosPanel.add(telefonoField);

        opcionesPanel = new JPanel();
        opcionesPanel.setLayout(new GridLayout(2,1));
        clienteButton = new JRadioButton("Cliente");
        repartidorButton = new JRadioButton("Repartidor");
        opcionesPanel.add(clienteButton);
        opcionesPanel.add(repartidorButton);

        opciones = new ButtonGroup();
        opciones.add(clienteButton);
        opciones.add(repartidorButton);

        aceptar = new JButton("Aceptar");
        cancelar = new JButton("Cancelar");
        botonesPanel = new JPanel();
        botonesPanel.add(aceptar);
        botonesPanel.add(cancelar);

        add(datosPanel, BorderLayout.WEST);//Nombre, rut...
        add(botonesPanel, BorderLayout.SOUTH);//Aceptar o cancelar
        add(opcionesPanel, BorderLayout.EAST);//Clientes o repartidor

        aceptar.addActionListener(this);
        cancelar.addActionListener(this);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if(event.getSource() == cancelar){
            dispose();
        }else if(event.getSource() == aceptar){
            String nombre = nombreField.getText();
            String rut = rutField.getText();
            String domicilio = domicilioField.getText();
            String telefono = telefonoField.getText();
            String clienteRepartidor = null;
            if(clienteButton.isSelected()) clienteRepartidor = "cliente";
            if(repartidorButton.isSelected()) clienteRepartidor = "repartidor";

            if(clienteRepartidor.equalsIgnoreCase("cliente")){
                if(validaRut(rut)){
                    try {
                        ControladorPedidosRestoran.getInstance().addCliente(rut,nombre,domicilio,telefono);
                        JOptionPane.showMessageDialog(null, "Cliente creado con exito",
                                "Resultado transaccion", JOptionPane.PLAIN_MESSAGE, icono("/Vista/pulgar.png",40,40));

                    } catch (ObjetoRepetidoException | OperacionInvalidaException e) {
                        JOptionPane.showMessageDialog(null, "Error! "+ e.getMessage(),
                                "Resultado transaccion", JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Error! El rut no es valido",
                            "Resultado transaccion", JOptionPane.ERROR_MESSAGE);
                }

            }else if(clienteRepartidor.equalsIgnoreCase("repartidor")){
                if(validaRut(rut)){
                    try {
                        ControladorPedidosRestoran.getInstance().addRepartidor(rut,nombre,domicilio,telefono);
                        JOptionPane.showMessageDialog(null, "Repartidor creado con exito",
                                "Resultado transaccion", JOptionPane.PLAIN_MESSAGE, icono("/Vista/pulgar.png",40,40));
                    } catch (ObjetoRepetidoException | OperacionInvalidaException e) {
                        JOptionPane.showMessageDialog(null, "Error! " + e.getMessage(),
                                "Resultado transaccion", JOptionPane.ERROR_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,"Error! El rut no es valido",
                            "Resultado transaccion", JOptionPane.ERROR_MESSAGE);
                }

            }else{
                JOptionPane.showMessageDialog(null, "Error, no ha selecionado cliente o repartidor",
                        "Resultado transaccion", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private Icon icono(String ubicacion, int ancho, int largo){
        Icon icon = new ImageIcon(new ImageIcon(getClass().getResource(ubicacion)).getImage().getScaledInstance(ancho, largo, java.awt.Image.SCALE_SMOOTH));
        return icon;
    }
}

