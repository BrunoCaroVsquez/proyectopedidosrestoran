package Vista;

import Controlador.ControladorPedidosRestoran;
import Excepciones.*;

import java.util.Scanner;
import java.util.zip.DataFormatException;
/*Aportaron: Sebastian Fierro S.
             Bruno Caro V.
             Natalia Carrasco C.
             Miguel Raibel B.
*/

public class UIPedidosRestoran {
    private static UIPedidosRestoran instance = null;
    private Scanner sc;
    private UIPedidosRestoran(){}

    public static UIPedidosRestoran getInstance(){
        if (instance == null){
            instance = new UIPedidosRestoran();
        }
        return instance;
    }

    public void menuPrincipal(){
        try {
            ControladorPedidosRestoran.getInstance().readDatosSistema();
        } catch (OperacionInvalidaException e) {
            creaDatosDePrueba();
        }

        sc = new Scanner(System.in).useDelimiter("\t|\r\n|[\n\r\u2028\u2029\u0085]");
        int opcion;
        do{
            System.out.println("\n\n...::::MENU PRINCIPAL::::...");
            System.out.println("1. Crear cliente/repartidor");
            System.out.println("2. Crear producto");
            System.out.println("3. Ingresar pedido");
            System.out.println("4. Ingresar entrega de pedidos");
            System.out.println("5. Listar productos");
            System.out.println("6. Listar repartidores");
            System.out.println("7. Listar pedidos pendientes");
            System.out.println("8. Listar pedidos por entregar");
            System.out.println("9. Listar pedidos mas solicitados");
            System.out.println("10. Salir");
            System.out.println(" ");
            System.out.println("\tIngrese opcion");

            opcion = sc.nextInt();

            switch (opcion){
                case 1 -> creaClienteRepartidor();
                case 2 -> creaProducto();
                case 3 -> ingresaPedidoCliente();
                case 4 -> ingresaEntregaPedido();
                case 5 -> listaProductos();
                case 6 -> listaRepartidores();
                case 7 -> listaPedidosPendientes();
                case 8 -> listaPedidosPorEntregar();
                case 9 -> listaProductosMasSolicitados();
                case 10 -> System.out.println("Saliendo...");
                default -> System.out.println("Error, la opcion ingresada no es valida");
            }
        }while (opcion !=10);
        sc.close();

        try {
            ControladorPedidosRestoran.getInstance().saveDatosSistema();
        } catch (OperacionInvalidaException e) {
            System.out.println("\nHubo un Problema: " + e.getMessage());
        }
    }
    private void creaClienteRepartidor(){
        GuiCreaClienteRepartidor guiCreaClienteRepartidor = new GuiCreaClienteRepartidor("Crea Cliente o Repartidor");
        guiCreaClienteRepartidor.inicia();
    }
    private void creaProducto() {
        int codigo, precioCosto, margenVentas, stock;
        int valido = 0;
        String nombre;
        do{
            System.out.println("*** CREACION DE UN NUEVO PRODUCTO ***");
            System.out.println("1. Crear Producto Basico");
            System.out.println("2. Crear Combo");
            System.out.print("Ingrese opcion: ");
            int select;
            select = sc.nextInt();
            switch (select){
                case 1 -> {
                    do {
                        System.out.print("Nombre: ");
                        nombre = sc.next();
                        if (nombre.isEmpty() || nombre.isBlank()) {
                            System.out.println("Ingrese nombre!");
                        }
                    } while (nombre.isEmpty() || nombre.isBlank());
                    System.out.print("Codigo: ");
                    codigo = sc.nextInt();
                    System.out.print("Precio de costo: ");
                    precioCosto = sc.nextInt();
                    System.out.print("Margen de venta: ");
                    margenVentas = sc.nextInt();
                    try {
                        ControladorPedidosRestoran.getInstance().addProductoBasico(codigo, nombre, precioCosto, margenVentas);
                        System.out.print("Stock inicial: ");
                        stock = sc.nextInt();
                        try {
                            ControladorPedidosRestoran.getInstance().addStockAProducto(codigo, stock);
                        } catch (Exception i) {
                            System.out.println("Hubo un Problema: " + i.getMessage());
                        }
                        System.out.println("Producto creado");
                        valido = 12;
                    } catch (Exception e) {
                        System.out.println("Hubo un Problema: " + e.getMessage());
                    }
                }
                case 2 ->{
                    do {
                        System.out.print("Nombre: ");
                        nombre = sc.next();
                        if (nombre.isEmpty() || nombre.isBlank()) {
                            System.out.println("Ingrese nombre!");
                        }
                    } while (nombre.isEmpty() || nombre.isBlank());
                    System.out.print("Codigo: ");
                    codigo = sc.nextInt();
                    System.out.print("Margen de venta: ");
                    margenVentas = sc.nextInt();
                    System.out.print("Indique el numero de productos en el combo: ");
                    int nroProduct = sc.nextInt();
                    while(nroProduct<=0){
                        System.out.println("Debe ser mayor que 0!");
                        nroProduct = sc.nextInt();
                    }
                    int [] listCodigos = new int [nroProduct];
                    int codigoBasico;
                    for(int i=0; i<listCodigos.length; i++){
                        System.out.print("\nCodigo del producto " + (i + 1) + ": ");
                        codigoBasico = sc.nextInt();
                        listCodigos[i] = codigoBasico;
                    }
                    try {
                        ControladorPedidosRestoran.getInstance().addCombo(codigo, nombre, margenVentas, listCodigos);
                        System.out.print("\nStock inicial: ");
                        stock = sc.nextInt();
                        try {
                            ControladorPedidosRestoran.getInstance().addStockAProducto(codigo, stock);
                        } catch (Exception i) {
                            System.out.println("\nHubo un Problema: " + i.getMessage());
                        }
                        System.out.println("\nProducto creado");
                        valido = 12;
                    } catch (Exception e) {
                        System.out.println("Hubo un Problema: " + e.getMessage());
                    }
                }
                default -> System.out.println("Ingrese una opcion valida");
            }

        }while(valido!=12);

    }

    private void ingresaPedidoCliente(){
        String rut;
        int nroProductosDist,codigoProducto,cantDelProducto;
        System.out.println("*** INGRESO DE UN PEDIDO ***");
        System.out.print("Rut cliente: ");
        rut=sc.next();
        String datosCliente[];
        try {
            datosCliente = ControladorPedidosRestoran.getInstance().getDatosCliente(rut);
            System.out.print("Nombre cliente: " + datosCliente[1]);
            System.out.print("\nIngrese el numero de productos distintos que incluye su pedido: ");
            nroProductosDist = sc.nextInt();
            while(nroProductosDist<=0){
                System.out.println("Debe ser mayor que 0!");
                nroProductosDist = sc.nextInt();
            }
            String datosProductos[][]= new String [nroProductosDist][2];
            for (int i = 0; i < nroProductosDist; i++) {
                System.out.print("\nCodigo del producto " + (i + 1) + ": ");
                codigoProducto = sc.nextInt();
                datosProductos[i][0] = String.valueOf(codigoProducto);
                System.out.println("Nombre producto: " + ControladorPedidosRestoran.getInstance().getDatosProducto(codigoProducto)[1]);
                System.out.println("Ingrese la cantidad que desea del producto en el pedido: ");
                cantDelProducto = sc.nextInt();
                datosProductos[i][1] = String.valueOf(cantDelProducto);
            }
            int comprobante;
            boolean check;
             System.out.println("¿Desea recibir un comprobante?(1=si, 0=no)");
             comprobante = sc.nextInt();
             if(comprobante == 1){
                 check = true;
             }else{
                 check = false;
             }
             int codigoPedido;
             codigoPedido = ControladorPedidosRestoran.getInstance().addPedido(rut,datosProductos, check);
             System.out.println("El codigo del pedido es: " + codigoPedido);
             System.out.print("\nIngrese el rut del repartidor: ");
             String rutRepartidor = sc.next();
             ControladorPedidosRestoran.getInstance().setRepartidorAPedido(rutRepartidor,codigoPedido);
             System.out.println("El pedido se ha realizado con exito");
        } catch (Exception e) {
            System.out.print("Hubo un problema: ");
            System.out.println(e.getMessage());
        }
    }

    private void ingresaEntregaPedido() {
        int pedidoEntregado;
        System.out.println("Ingrese el numero del pedido a entregar");
        pedidoEntregado = sc.nextInt();
        try {
            ControladorPedidosRestoran.getInstance().setEntregaPedido(pedidoEntregado);
        } catch (Exception e) {
            System.out.print("Hubo un problema: ");
            System.out.println(e.getMessage());
        }
    }

    private void listaProductos(){
        System.out.println("*** LISTADO DE PRODUCTOS ***");
        System.out.printf("%6s %-25s %-20s %-15s %-15s %-11s%n","Codigo","Nombre","Costo","Margen","Stock","Componentes");
        System.out.println("--------------------------------------------------------------------------------------------------");
        String [][] listado = ControladorPedidosRestoran.getInstance().listProductos();
        if (listado.length > 0){
            for (String[] productos: listado) {
                System.out.printf("%-6s %-25s %-20s %-15s %-15s %-11s%n",productos[0],productos[1],productos[2],productos[3],productos[4],productos[5]);
            }
        }else{
            System.out.println("No hay productos creados");
        }
    }
    private void listaRepartidores(){
        System.out.println("*** LISTADO DE REPARTIDORES ***");
        System.out.printf("%-15s %-25s %-40s %-15s%n" ,"Rut","Nombre","Domicilio","Telefono");
        System.out.println("-----------------------------------------------------------------------------------------------------");
        String [][] listadoRepartidor = ControladorPedidosRestoran.getInstance().listRepartidores();
        if (listadoRepartidor.length > 0){
            for (String [] repartidores : listadoRepartidor){
                System.out.printf("%-15s %-25s %-40s %-15s%n",repartidores[0],repartidores[1],repartidores[2],repartidores[3]);
            }
        }else{
            System.out.println("No hay Repartidores creados");
        }
    }
    private void listaPedidosPendientes(){
        System.out.println("*** LISTADO DE PEDIDOS PENDIENTES DE UN CLIENTE ***");
        System.out.print("Rut cliente: ");
        String rutCliente = sc.next();
        String[][] pedidosPendientes;
        try {
            pedidosPendientes = ControladorPedidosRestoran.getInstance().listPedidosPendientes(rutCliente);
            System.out.printf("%-6s %-25s %-30s %-20s%n", "Numero", "Fecha", "Rut repartidor", "Nombre repartidor");
            System.out.println("-----------------------------------------------------------------------------------------------------");
            if(pedidosPendientes.length > 0){
                for (String[] pedidos : pedidosPendientes) {
                    System.out.printf("%6s %-25s %-30s %-30s%n", pedidos[0], pedidos[1], pedidos[2], pedidos[3]);
                }
            }else{
                System.out.println("El cliente no tiene pedidos pendientes");
            }

        } catch (ClienteNoEncontradoException e) {
            System.out.print("Ocurrio un error: ");
            System.out.println(e.getMessage());
        }
    }
    private void listaPedidosPorEntregar(){
        String rutRepartidor;
        System.out.println("*** LISTADO DE PEDIDOS POR ENTREGAR DE UN REPARTIDOR ***");
        System.out.print("Rut repartidor: ");
        rutRepartidor = sc.next();
        String[][] pedidosPendientes;
        try {
            pedidosPendientes = ControladorPedidosRestoran.getInstance().listPedidosPorEntregar(rutRepartidor);
            System.out.printf("%-7s %-25s %-30s %-20s%n", "Numero", "Fecha", "Rut cliente", "Nombre cliente");
            System.out.println("-----------------------------------------------------------------------------------------------------");
            if(pedidosPendientes.length > 0){
                for (String[] pedidosAEntregar : pedidosPendientes) {
                    System.out.printf("%7s %-25s %-30s %-20s%n", pedidosAEntregar[0], pedidosAEntregar[1], pedidosAEntregar[2], pedidosAEntregar[3]);
                }
            }else{
                System.out.println("El repartidor no tiene pedidos por entregar");
            }

        } catch (RepartidorNoEncontradoException e) {
            System.out.print("Ocurrio un error: ");
            System.out.println(e.getMessage());
        }
    }
    private void listaProductosMasSolicitados(){
        int nroPedidosMin;
        System.out.println("*** LISTADO DE PRODUCTOS MAS SOLICITADOS \t***");
        System.out.println("Nro minimo de pedido: ");
        nroPedidosMin = sc.nextInt();
        String [][] productoSoli;
        try {
            productoSoli = ControladorPedidosRestoran.getInstance().ProductosMasSolicitados(nroPedidosMin);
            System.out.printf("%-7s %-25s %-12s %-12s%n", "Codigo", "Nombre", "Precio Venta", "Nro. Pedidos");
            System.out.println("----------------------------------------------------------");
            if(productoSoli.length > 0){
                for(String[] productosPopulares : productoSoli) {
                    System.out.printf("%7s %-25s %-12s %-12s%n", productosPopulares[0], productosPopulares[1], productosPopulares[2], productosPopulares[3]);
                }
            }else{
                System.out.println("No hay productos con el minimo de pedido");
            }

        } catch (Exception e) {
            System.out.println("Hubo un Problema: " + e.getMessage());
        }
    }
    private void creaDatosDePrueba() {
        try {
            ControladorPedidosRestoran.getInstance().addCliente("20.500.745-8","Juan Perez", "123 calle x", "+56988753415");
        } catch (ObjetoRepetidoException | OperacionInvalidaException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addCliente("18.183.123-5", "Luis Jara", "Rio viejo 520", "+56912456543");
        } catch (ObjetoRepetidoException | OperacionInvalidaException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addProductoBasico(1, "Completo", 900, 25);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ObjetoRepetidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addProductoBasico(2, "Jugo de naranja", 450, 60);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ObjetoRepetidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addProductoBasico(3, "Papas fritas", 400, 50);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ObjetoRepetidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }

        try {
            ControladorPedidosRestoran.getInstance().addRepartidor("19.456.891-6", "Alejandra Ramirez", "Calle 2", "+56912345623");
        } catch (ObjetoRepetidoException | OperacionInvalidaException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addStockAProducto(1, 300);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ProductoNoEncontradoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addStockAProducto(2, 600);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ProductoNoEncontradoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }
        try {
            ControladorPedidosRestoran.getInstance().addStockAProducto(3, 120);
        } catch (ValorInvalidoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        } catch (ProductoNoEncontradoException e) {
            System.out.println("Hubo un problema: " + e.getMessage());
        }

    }

}

